#include "modules/dialogue_parser.hh"
#include "modules/dialogue_sequencer.hh"
#include "modules/sequence_extractors.hh"
#include "modules/sequence_alignment.hh"
#include "modules/similarity_functions.hh"

#include <iostream>
#include <sstream>

decoda::Sequence StringToSequence(std::string str) {
  decoda::Sequence sequence;

  sequence.resize(str.length());

  for (size_t k = 0; k < str.length(); ++k) {
    std::stringstream ss;
    std::string letter;
    ss << str[k];
    ss >> letter;
    sequence[k].push_back(letter);
  }

  return sequence;
}

// std::pair<decoda::Sequence, decoda::Sequence> CoordinatesToSubSequences(decoda::Sequence &a,
// 							decoda::Sequence &b,
// 							std::vector<std::pair<int,int> > coordinates){
//   decoda::Sequence sub_a;
//   decoda::Sequence sub_b;

//   sub_a.resize(coordinates.size());
//   sub_b.resize(coordinates.size());
  
//   for (size_t k = 0; k < coordinates.size(); ++k) {
//     sub_a[k].push_back(coordinates[k].first);
//     sub_b[k].push_back(coordinates[k].second);
//   }


//   return 
// }

std::string SequenceToString(decoda::Sequence &sequence) {
  std::string str;

  for (auto &tuple : sequence) {
    str += boost::get<std::string>(tuple[0]);
  }

  return str;
}

int main(int argc, char *argv[]) {

  std::vector<decoda::Similarity> similarity_funcs;
  similarity_funcs.push_back(decoda::identity_similarity);

  decoda::Sequence a = StringToSequence("CCAATCTACTACTGCTTGCAGTAC");
  decoda::Sequence b = StringToSequence("AGTCCGAGGGCTACTCTACTGAAC");

  auto alignments = decoda::WatermanEggert(a, b, similarity_funcs, 0, 2, 2);

  std::cout << "Nb sequences (>2): " << alignments.size() << std::endl;

  for (size_t k = 0; k < alignments.size(); k++) {
    std::cout << "Alignment " << k << ": score=" << alignments[k].score << ", cigar="
	      << alignments[k].cigar << ", Coords=(" << alignments[k].topleft.first
	      << "," << alignments[k].topleft.second << ")" <<  std::endl;
  }
  
  return 0;
}
