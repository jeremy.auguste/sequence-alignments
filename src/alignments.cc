#include "modules/dialogue_parser.hh"
#include "modules/dialogue_sequencer.hh"
#include "modules/sequence_extractors.hh"
#include "modules/sequence_alignment.hh"
#include "modules/similarity_functions.hh"

#include <getopt.h>
#include <cmath>

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <map>
#include <set>
#include <utility>
#include <boost/lexical_cast.hpp>

int verbose = 0;

std::pair<decoda::Extractor, decoda::Similarity> ExtractorFromString(std::string str) {
  if (str == "dialogueacts")
    return {decoda::DialogueActs, decoda::identity_similarity};
  if (str == "polarity")
    return {decoda::TurnMeanPolarity, decoda::continuous_similarity};
  if (str == "speaker")
    return {decoda::Speaker, decoda::speaker_similarity};
  if (str == "responsetime")
    return {decoda::ResponseTime, decoda::continuous_similarity};
  if (str == "length")
    return {decoda::TurnLength, decoda::identity_similarity};

  throw std::invalid_argument(str + " is not a valid extractor");
}

void HelpMessage(std::ostream &output, const std::string prog_name) {
  output << "Usage: " << prog_name
		 << " [OPTIONS] -x mode -d stem_dir extractor1 [extractor2 ...]"
		 << std::endl;
  output << "Options (All):" << std::endl;
  output << "\t-o output -- Main output file (affinity matrix or dictionnary)" << std::endl;
  output << "\t-m dialoguemap -- Output file to store the dialogue mapping in" << std::endl;
  output << "\t-v -- Adds one level to the verbose level" << std::endl;
  output << "\t-i -- The initial gap penality" << std::endl;
  output << "\t-e -- The extended gap penality" << std::endl;
  output << "Options (Mode 0):" << std::endl;
  output << "\t-a align_method -- Alignment method to use (required with mode 0)" << std::endl;
  output << "Options (Mode 1 and 2):" << std::endl;
  output << "\t--perfectmatch -- Only allows perfect matches in dictionnary keys" << std::endl;
  output << "\t--partialmatch -- Allow some gaps and mismatches in dictionnary keys" << std::endl;
  output << "Modes:" << std::endl;
  output << "\t 0: Builds an affinity matrix" << std::endl;
  output << "\t 1: Builds an alignment dictionnary" << std::endl;
  output << "\t 2: Builds vectors for each dialogue" << std::endl;
  output << "Align Methods: 'smithwaterman' or 'needlemanwunsch'" << std::endl;
  output << "Extractors: 'dialogueacts', 'polarity', 'speaker' and 'length'"
		 << std::endl;
}

void BuildAffinityMatrix(std::vector<decoda::Sequence> &sequences,
						 std::vector<decoda::Similarity> &similarity_funcs,
						 double init_gap, double extend_gap,
						 decoda::AlignmentFunc alignment_func,
						 std::ostream &output) {
  int nb_dialogues = sequences.size();

  if (verbose)
    std::cerr << "Building affinity matrix..." << std::endl;
  
  int count = 0;
  for (int i = 0; i < nb_dialogues; i++) {
    for (int j = i; j < nb_dialogues; j++) {
      if (verbose > 1 && count % 100 == 0) { 
      	std::cerr << "Progress: " << count << "/"
				  << (nb_dialogues+1)*nb_dialogues / 2 << "\r";
      	std::cerr.flush();
      }
      count++;
      output << decoda::AlignmentSimilarity(sequences[i], sequences[j],
											similarity_funcs,
											init_gap, extend_gap, alignment_func) << " ";
    }
    output << std::endl;
  }

  if (verbose > 1)
    std::cerr << std::endl << "Done." << std::endl;
}

std::string SequenceToString(const decoda::Sequence &sequence) {
  std::string str = "";
  bool sfirst = true;
  for (auto &tuple : sequence) {
    bool first = true;
    if (!sfirst)
      str += ";";
    str += "(";
    for (auto &value : tuple) {
      if (first)
		str += boost::lexical_cast<std::string>(value);
      else
		str += "," + boost::lexical_cast<std::string>(value);
      first = false;
    }
    str += ")";
    sfirst = false;
  }

  return str;
}

void AddAlignmentToDictionnary(decoda::Alignment &alignment,
							   std::map<decoda::Sequence, int> &dictionnary) {
  decoda::Sequence sequence;
  std::vector<decoda::ExtractorTypes> mismatch;

  int s1_idx = alignment.topleft.first;

  for (auto &action : alignment.cigar) {
    switch (action) {
    case 'M': {
      sequence.push_back(alignment.a[s1_idx]);
      s1_idx++;
      break;
    }
    case 'X': {
      sequence.push_back(mismatch);
      s1_idx++;
      break;
    }
    case 'D': {
      s1_idx++;
      break;
    }
    }
  }

  if (dictionnary.count(sequence)) {
    dictionnary[sequence]++;
  } else {
    dictionnary[sequence] = 1;
  }
}

void AddSubAlignmentsToDictionnary(decoda::Alignment &alignment,
								   std::map<decoda::Sequence, int> &dictionnary) {
  int s1_idx = alignment.topleft.first;
	
  decoda::Sequence subsequence;
  for (auto &action : alignment.cigar) {
    if (action == 'M') {
      subsequence.push_back(alignment.a[s1_idx]);
      s1_idx++;
    } else {
      if (!subsequence.empty()) {
		if (dictionnary.count(subsequence)) {
		  dictionnary[subsequence]++;
		} else {
		  dictionnary[subsequence] = 1;
		}

		subsequence.clear();
      }
      if (action == 'D' || action == 'X') {
		s1_idx++;
      }
    }
  }

  if (!subsequence.empty()) {
    if (dictionnary.count(subsequence)) {
      dictionnary[subsequence]++;
    } else {
      dictionnary[subsequence] = 1;
    }
  }
}

void BuildAlignmentDictionnary(std::vector<decoda::Sequence> &sequences,
							   std::vector<decoda::Similarity> &similarity_funcs,
							   double init_gap, double extend_gap, double min_score,
							   std::ostream &output, bool perfect_match) {
  size_t nb_dialogues = sequences.size();

  if (verbose)
    std::cerr << "Building dictionnary..." << std::endl;

  std::map<decoda::Sequence, int> dictionnary;
  int count = 0;
  for (size_t i = 0; i < nb_dialogues; ++i) {
    for (size_t j = i+1; j < nb_dialogues; ++j) {
      if (verbose > 1 && count % 100 == 0) { 
      	std::cerr << "Progress: " << count << "/"
				  << (nb_dialogues+1)*nb_dialogues / 2 - nb_dialogues << "\r";
      	std::cerr.flush();
      }
      count++;
      std::vector<decoda::Alignment> alignments = decoda::WatermanEggert(sequences[i], sequences[j],
																		 similarity_funcs,
																		 init_gap, extend_gap,
																		 min_score);
      for (decoda::Alignment &alignment : alignments) {
		if (perfect_match)
		  AddSubAlignmentsToDictionnary(alignment, dictionnary);
		else
		  AddAlignmentToDictionnary(alignment, dictionnary);
      }
    }
  }

  if (verbose > 1)
    std::cerr << std::endl << "Done." << std::endl;

  if (verbose)
    std::cerr << "Dictionnary size: " << dictionnary.size() << std::endl;

  for (auto &kv : dictionnary) {
    output << SequenceToString(kv.first) << "\t" << kv.second << std::endl;
  }
}

void AddAlignmentToVectors(decoda::Alignment alignment,
						   std::vector<decoda::Similarity> &similarity_funcs,
						   std::map<std::string, std::set<int>> &vectors,
						   int dlg1, int dlg2) {
  std::string sequence1 = "";
  std::string sequence2 = "";
  
  int s1_idx = alignment.topleft.first;
  int s2_idx = alignment.topleft.second;
  bool first = true;
  
  for (auto &action : alignment.cigar) {
    switch (action) {
    case 'M':
    case 'X': {
      size_t n_extractors = alignment.a[s1_idx].size();
      if (!first) {
		sequence1 += ";";
		sequence2 += ";";
      }
      first = false;
      sequence1 += "(";
      sequence2 += "(";
      for (size_t k = 0; k < n_extractors; k++) {
		if (similarity_funcs[k] == decoda::continuous_similarity) {
		  double sim = decoda::continuous_similarity(alignment.a[s1_idx][k], alignment.b[s2_idx][k]);
		  if (sim >= 0.0) {
			long int_a = std::lround(boost::lexical_cast<double>(alignment.a[s1_idx][k]));
			long int_b = std::lround(boost::lexical_cast<double>(alignment.b[s2_idx][k]));

			sequence1 += std::to_string(int_a);
			sequence2 += std::to_string(int_b);
		  }
		} else {
		  if (alignment.a[s1_idx][k] == alignment.b[s2_idx][k]) {
			std::stringstream ss;
			ss << alignment.a[s1_idx][k];
			sequence1 += ss.str();
			sequence2 += ss.str();
		  }
		}
	
		if (k != n_extractors-1) {
		  sequence1 += ",";
		  sequence2 += ",";
		}
      }

      sequence1 += ")";
      sequence2 += ")";
      
      s1_idx++;
      s2_idx++;
      break;
    }
    case 'D': {
      s1_idx++;
      break;
    }
    case 'I': {
      s2_idx++;
      break;
    }
    }
  }

  vectors[sequence1].insert(dlg1);
  vectors[sequence1].insert(dlg2);
  vectors[sequence2].insert(dlg1);
  vectors[sequence2].insert(dlg2);
}

void AddSubAlignmentsToVectors(decoda::Alignment alignment,
							   std::vector<decoda::Similarity> &similarity_funcs,
							   std::map<std::string, std::set<int>> &vectors,
							   int dlg1, int dlg2) {
  int s1_idx = alignment.topleft.first;
  int s2_idx = alignment.topleft.second;
	
  std::string sequence1 = "";
  std::string sequence2 = "";
  bool first = true;
  
  for (auto &action : alignment.cigar) {
    if (action == 'M' || action == 'X') {
      size_t n_extractors = alignment.a[s1_idx].size();
      std::string substr1 = "(";
      std::string substr2 = "(";
      bool completed = true;

      for (size_t k = 0; k < n_extractors; k++) {
		if (similarity_funcs[k] == decoda::continuous_similarity) {
		  double sim = decoda::continuous_similarity(alignment.a[s1_idx][k], alignment.b[s2_idx][k]);
		  if (sim >= 0.0) {
			long int_a = std::lround(boost::lexical_cast<double>(alignment.a[s1_idx][k]));
			long int_b = std::lround(boost::lexical_cast<double>(alignment.b[s2_idx][k]));

			substr1 += std::to_string(int_a);
			substr2 += std::to_string(int_b);
		  } else {
			completed = false;
			break;
		  }
		} else {
		  if (alignment.a[s1_idx][k] == alignment.b[s2_idx][k]) {
			std::stringstream ss;
			ss << alignment.a[s1_idx][k];
			substr1 += ss.str();
			substr2 += ss.str();
		  } else {
			completed = false;
			break;
		  }
		}
	
		if (k != n_extractors-1) {
		  substr1 += ",";
		  substr2 += ",";
		}
      }
      substr1 += ")";
      substr2 += ")";

      if (completed) {
		if (!first) {
		  sequence1 += ";";
		  sequence2 += ";";
		}
		first = false;
		sequence1 += substr1;
		sequence2 += substr2;
      } else {
		if (!sequence1.empty()) {
		  vectors[sequence1].insert(dlg1);
		  vectors[sequence1].insert(dlg2);
		  vectors[sequence2].insert(dlg1);
		  vectors[sequence2].insert(dlg2);
		  sequence1.clear();
		  sequence2.clear();
		}
		first = true;
      }

      s1_idx++;
      s2_idx++;
    } else {
      if (!sequence1.empty()) {
		vectors[sequence1].insert(dlg1);
		vectors[sequence1].insert(dlg2);
		vectors[sequence2].insert(dlg1);
		vectors[sequence2].insert(dlg2);
		first = true;
		sequence1.clear();
		sequence2.clear();
      }
      if (action == 'D') {
		s1_idx++;
      }
      if (action == 'I') {
		s2_idx++;
      }
    }
  }

  if (!sequence1.empty()) {
    vectors[sequence1].insert(dlg1);
    vectors[sequence1].insert(dlg2);
    vectors[sequence2].insert(dlg1);
    vectors[sequence2].insert(dlg2);
  }
}

void BuildDialogueVectors(std::vector<decoda::Sequence> &sequences,
						  std::vector<decoda::Similarity> &similarity_funcs,
						  double init_gap, double extend_gap, double min_score,
						  std::ostream &output, bool perfect_match) {

  size_t nb_dialogues = sequences.size();

  if (verbose)
    std::cerr << "Building vectors..." << std::endl;

  std::map<std::string, std::set<int>> vectors;
  int count = 0;
  for (size_t i = 0; i < nb_dialogues; ++i) {
    for (size_t j = i+1; j < nb_dialogues; ++j) {
      if (verbose > 1 && count % 100 == 0) { 
      	std::cerr << "Progress: " << count << "/"
				  << (nb_dialogues+1)*nb_dialogues / 2 - nb_dialogues << "\r";
      	std::cerr.flush();
      }
      count++;
      std::vector<decoda::Alignment> alignments = decoda::WatermanEggert(sequences[i], sequences[j],
																		 similarity_funcs,
																		 init_gap, extend_gap,
																		 min_score);
      for (decoda::Alignment &alignment : alignments) {
		if (perfect_match) {
		  AddSubAlignmentsToVectors(alignment, similarity_funcs, vectors, i, j);
		} else {
		  AddAlignmentToVectors(alignment, similarity_funcs, vectors, i, j);
		}
      }
    }
  }

  if (verbose > 1)
    std::cerr << std::endl << "Done." << std::endl;

  for (auto &kv : vectors) {
    output << kv.first;
    for (int value : vectors[kv.first]) {
      output << "\t" << value;
    }
    output << std::endl;
  }
}

int main(int argc, char *argv[]) {
  std::string stem_dir;
  bool dir_set = false;
  decoda::AlignmentFunc alignment_func;
  std::string align_method;
  bool align_func_set = false;
  std::streambuf *buf = NULL;
  std::ofstream of;
  std::string dialoguemap;
  bool dialoguemap_set = false;
  int mode;
  int perfect_match = 0;
  double init_gap = 0;
  double extend_gap = 1;
  
  for (;;) {
    static struct option long_options[] =
      {
		{"help", no_argument, 0, 'h'},
		{"mode", required_argument, 0, 'x'},
		{"alignmethod", required_argument, 0, 'a'},
		{"dir", required_argument, 0, 'd'},
		{"output", required_argument, 0, 'o'},
		{"dialoguemap", required_argument, 0, 'm'},
		{"perfectmatch", no_argument, &perfect_match, 1},
		{"partialmatch", no_argument, &perfect_match, 0},
		{"verbose", no_argument, 0, 'v'},
		{"init-gap", required_argument, 0, 'i'},
		{"extend-gap", required_argument, 0, 'e'},
		{0, 0, 0, 0}
      };

    int option_index = 0;

    int c = getopt_long(argc, argv,"ha:d:o:m:x:i:e:v", long_options, &option_index);

    if (c == -1)
      break;

    switch (c) {
    case 'h': {
      HelpMessage(std::cout, argv[0]);
      return 0;
    }
    case 'a': {
      align_method = optarg;
      alignment_func = decoda::AlignmentFuncFromString(align_method);
      align_func_set = true;
      break;
    }
    case 'd': {
      stem_dir = optarg;
      dir_set = true;
      break;
    }
    case 'o': {
      of.open(optarg);
      buf = of.rdbuf();
      break;
    }
    case 'm': {
      dialoguemap = optarg;
      dialoguemap_set = true;
      break;
    }
    case 'x': {
      try {
		mode = std::stoi(optarg);
      } catch (std::exception &e) {
		std::cerr << "Argument of -x '" << optarg << "' is not an integer!";
		exit(1);
      }
      break;
    }
    case 'i': {
      try {
		init_gap = std::stod(optarg);
      } catch (std::exception &e) {
		std::cerr << "Argument of -i '" << optarg << "' is not a float!";
		exit(1);
      }
      break;
    }
    case 'e': {
      try {
		extend_gap = std::stod(optarg);
      } catch (std::exception &e) {
		std::cerr << "Argument of -e '" << optarg << "' is not a float!";
		exit(1);
      }
      break;
    }
    case 'v': {
      verbose++;
      break;
    }
    default: {
      break;
    }
    }
  }

  switch(mode) {
  case 0: {
    if (!dir_set || !align_func_set || optind >= argc) {
      HelpMessage(std::cerr, argv[0]);
      return 1;
    }
    break;
  }
  case 1:
  case 2: {
    if (!dir_set || optind >= argc) {
      HelpMessage(std::cerr, argv[0]);
      return 1;
    }
    break;
  }
  default: {
    std::cerr << "Mode '" << mode << "' is an unknown mode!" << std::endl;
    return 1;
  }
  }

  if (buf == NULL) {
    buf = std::cout.rdbuf();
  }
  
  std::ostream output(buf);
  
  std::map<std::string, std::pair<decoda::Extractor, decoda::Similarity> > extractors_map;

  for (int k = optind; k < argc; k++) {
    extractors_map.insert({argv[k], ExtractorFromString(argv[k])});
  }

  if (verbose)
    std::cerr << "Parsing dialogues..." << std::endl;
  decoda::DialogueParser parser(stem_dir);

  parser.ReadAll();

  if (dialoguemap_set) {
    if (verbose)
      std::cerr << "Outputing dialogue map..." << std::endl;
    std::ofstream odialoguemap(dialoguemap);

    for (size_t k = 0; k < parser.index2dialogue.size(); k++) {
      odialoguemap << parser.index2dialogue[k] << std::endl;
    }

    odialoguemap.close();
  }

  if (verbose)
    std::cerr << "Number of dialogues: " << parser.dialogues.size() << std::endl;

  std::vector<decoda::Extractor> extractors;
  std::vector<decoda::Similarity> similarity_funcs;

  if (verbose) {
    if (mode == 0)
      std::cerr << "Alignment method being used: " << align_method << std::endl;
    else if (mode == 1)
      std::cerr << "Alignment method being used: WatermanEggert" << std::endl;
  }

  if (verbose)
    std::cerr << "Extractors being used: ";
  for (auto &kv : extractors_map) {
    if (verbose)
      std::cerr << kv.first << " ";
    extractors.push_back(kv.second.first);
    similarity_funcs.push_back(kv.second.second);
  }
  if (verbose)
    std::cerr << std::endl;

  if (verbose) {
    std::cerr << "Gap penality affine function: " << init_gap << " + " << extend_gap << "*k" << std::endl;
  }
  
  
  decoda::DialogueSequencer sequencer(extractors);

  auto sequences = sequencer.TransformAll(parser.dialogues);

  if (mode == 0) {
    BuildAffinityMatrix(sequences, similarity_funcs, init_gap, extend_gap, alignment_func, output);
  } else if (mode == 1) {
    BuildAlignmentDictionnary(sequences, similarity_funcs, init_gap, extend_gap, 3, output, perfect_match);
  } else if (mode == 2) {
    BuildDialogueVectors(sequences, similarity_funcs, init_gap, extend_gap, 3, output, perfect_match);
  }
  
  return 0;
}
