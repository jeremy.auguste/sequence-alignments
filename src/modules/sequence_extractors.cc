#include "sequence_extractors.hh"

#include <math.h>
#include <exception>

namespace decoda {
  std::vector<ExtractorTypes> DialogueActs(Dialogue &dialogue) {
	std::vector<ExtractorTypes> partial_sequence;

	for (Turn &turn : dialogue) {
	  if (turn[0][kAct] == "//") {
		continue;
	  }
			
	  std::string act = turn[0][kAct];
	  partial_sequence.push_back(act);
	}
		
	return partial_sequence;
  }

  std::vector<ExtractorTypes> TurnLength(Dialogue &dialogue) {
	std::vector<ExtractorTypes> partial_sequence;

	int length = 0;
	for (Turn &turn : dialogue) {
	  if (turn[0][kAct] == "//") {
		continue;
	  }
	  length = turn.size();
	  length = (length-1) / 3;
	  if (length > 5)
		length = 5;
	  partial_sequence.push_back(length);
	  // length = 0;
	}

	// if (length != 0) {
	//   length = (length-1) / 3;
	//   if (length > 5)
	// 	length = 5;
	//   partial_sequence.push_back(length);
	// }

	return partial_sequence;
  }

  std::vector<ExtractorTypes> TurnMeanPolarity(Dialogue &dialogue) {
	std::vector<ExtractorTypes> partial_sequence;
		
	for (Turn &turn : dialogue) {
	  if (turn[0][kAct] == "//") {
		continue;
	  }
			
	  double total_polarity = 0.0;
	  int nb_words = turn.size();
	  for (Word &word : turn) {
		total_polarity += std::stod(word[kPolarity])*2.5; // unit is 0.4 --> 0.4*2.5 = 1
	  }
	  double mean = total_polarity / nb_words;
	  partial_sequence.push_back(mean);
	}

	return partial_sequence;
  }

  std::vector<ExtractorTypes> TurnMeanPolarityDiscrete(Dialogue &dialogue) {
	std::vector<ExtractorTypes> partial_sequence;
		
	for (Turn &turn : dialogue) {
	  if (turn[0][kAct] == "//") {
		continue;
	  }
			
	  double total_polarity = 0.0;
	  int nb_words = turn.size();
	  for (Word &word : turn) {
		total_polarity += std::stod(word[kPolarity]);
	  }
	  int mean = nearbyint((total_polarity / nb_words) / 0.4);
	  partial_sequence.push_back(mean);
			
	}

	return partial_sequence;
  }

  std::vector<ExtractorTypes> Speaker(Dialogue &dialogue) {
	std::vector<ExtractorTypes> partial_sequence;

	for (Turn &turn : dialogue) {
	  if (turn[0][kAct] == "//") {
		continue;
	  }

	  std::string speaker = turn[0][kSpkResolved];
	  partial_sequence.push_back(speaker);
	}
		
	return partial_sequence;
  }

  std::vector<ExtractorTypes> ResponseTime(Dialogue &dialogue) {
	std::vector<ExtractorTypes> partial_sequence;

	double prev_timestamp = 0.0;

	for (Turn &turn : dialogue) {
	  double current_timestamp = std::stod(turn[0][kStart]);
	  double delay = current_timestamp - prev_timestamp;
	  if (prev_timestamp == 0.0) // It's the first turn
		delay = 0.0;
	  prev_timestamp = std::stod(turn.back()[kEnd]);
	  if (turn[0][kAct] == "//") {
		continue;
	  }

	  partial_sequence.push_back(delay);
	}

	return partial_sequence;
  }
}
