#ifndef MODULES_DECISION_RESOLVER_H_
#define MODULES_DECISION_RESOLVER_H_

#include "sequence_extractors.hh"

#include <string>
#include "boost/lexical_cast.hpp"

namespace decoda {

  typedef std::string (*Resolver)(ExtractorTypes&, ExtractorTypes&, bool);

  std::string BinaryDecisionResolver(decoda::ExtractorTypes &a, decoda::ExtractorTypes &b,
				     bool strict) {
    if (a == b) return boost::lexical_cast<std::string>(a);
    return "";
  }

  std::string ContinuousDecisionResolver(decoda::ExtractorTypes &a, decoda::ExtractorTypes &b,
					 bool strict=false) {
    double double_a = boost::lexical_cast<double>(a);
    double double_b = boost::lexical_cast<double>(b);
    long value = std::lround(std::abs(double_a - double_b));
    if (!strict)
      return std::to_string(value);
  }
  
}
#endif /* MODULES_DECISION_RESOLVER_H_ */
