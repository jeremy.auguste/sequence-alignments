#include "dialogue_sequencer.hh"
#include <exception>

namespace decoda {
  DialogueSequencer::DialogueSequencer(std::vector<Extractor> extractors) : extractors(extractors) {}

  Sequence DialogueSequencer::Transform(Dialogue &dialogue) {
    Sequence sequence;
	size_t size = 0;
	
    for (Extractor extractor : extractors) {
      std::vector<ExtractorTypes> extracted = extractor(dialogue);

      if (sequence.empty()) {
		sequence.resize(extracted.size());
		size = extracted.size();
      }

	  if (size != extracted.size()) {
		throw std::runtime_error("Extractors don't extract sequences of the same length !");
	  }
      
      for (size_t k = 0; k < extracted.size(); ++k) {
		sequence[k].push_back(extracted[k]);
      }
    }
    
    return sequence;
  }

  std::vector<Sequence> DialogueSequencer::TransformAll(std::vector<Dialogue> &dialogues) {
    std::vector<Sequence> sequences;
	
    for (Dialogue dialogue : dialogues) {
      sequences.push_back(Transform(dialogue));
    }

    return sequences;
  }
}
