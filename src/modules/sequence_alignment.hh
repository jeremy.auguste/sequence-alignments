#ifndef MODULES_SEQUENCE_ALIGNMENT_H_
#define MODULES_SEQUENCE_ALIGNMENT_H_

#include "dialogue_sequencer.hh"
#include "similarity_functions.hh"

#include <vector>
#include <string>

namespace decoda {

  typedef double (*AlignmentFunc)(Sequence&, Sequence&,
				  std::vector<Similarity>&,
				  double, double);

  using Coordinates=std::pair<int,int>;

  struct Alignment {
  public:
    Sequence &a;
    Sequence &b;
    double score;
    Coordinates topleft;
    std::string cigar;

    Alignment(Sequence &a, Sequence &b, double score,
	      Coordinates topleft, std::string cigar) : a(a), b(b), score(score),
							topleft(topleft), cigar(cigar) {}
  };
  
  enum PathOrientations {
    kPathNone = 0x00,
    kPathDiagonal = 0x01,
    kPathUp = 0x02,
    kPathLeft = 0x04
  };

  AlignmentFunc AlignmentFuncFromString(std::string str);
  
  double SmithWaterman(Sequence &a, Sequence &b,
		       std::vector<Similarity> &similarity_funcs,
		       double init_gap_penality=0,
		       double extend_gap_penality=1);
  double NeedlemanWunsch(Sequence &a, Sequence &b,
			 std::vector<Similarity> &similarity_funcs,
			 double init_gap_penality=0,
			 double extend_gap_penality=1);
  double AlignmentSimilarity(Sequence &a, Sequence &b,
			     std::vector<Similarity> &similarity_funcs,
			     double init_gap_penality,
			     double extend_gap_penality,
			     AlignmentFunc alignment_func);

  std::vector<Alignment> WatermanEggert(Sequence &a, Sequence &b,
					std::vector<Similarity> &similarity_funcs,
					double init_gap_penality,
					double extend_gap_penality,
					double min_score);
}

#endif /* MODULES_SEQUENCE_ALIGNMENT_H_ */
