#ifndef MODULES_SIMILARITY_FUNCTIONS_H_
#define MODULES_SIMILARITY_FUNCTIONS_H_

#include "sequence_extractors.hh"

#include <vector>
#include <string>

namespace decoda {
  typedef double (*Similarity)(ExtractorTypes&, ExtractorTypes&);

  double identity_similarity(ExtractorTypes &a, ExtractorTypes &b);
  double speaker_similarity(ExtractorTypes &a, ExtractorTypes &b);
  double response_time_similarity(ExtractorTypes &a, ExtractorTypes &b);
  double continuous_similarity(ExtractorTypes &a, ExtractorTypes &b);

  double TotalSimilarity(std::vector<ExtractorTypes> &a, std::vector<ExtractorTypes> &b,
			  std::vector<Similarity> similarity_funcs);
}

#endif /* MODULES_SIMILARITY_FUNCTIONS_H_ */
