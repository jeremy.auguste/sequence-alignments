#include "similarity_functions.hh"

#include <cmath>

namespace decoda {
  double identity_similarity(ExtractorTypes &a, ExtractorTypes &b) {
    if (a == b) return 1.0;
    return -0.9;
  }

  double speaker_similarity(ExtractorTypes &a, ExtractorTypes &b) {
    if (a == b) return 0.0;
    return -1.9;
  }

  double continuous_similarity(ExtractorTypes &a, ExtractorTypes &b) {
    double double_a = boost::get<double>(a);
    double double_b = boost::get<double>(b);

    return 1.0 - std::abs(double_a - double_b);
  }

  double TotalSimilarity(std::vector<ExtractorTypes> &a, std::vector<ExtractorTypes> &b,
			  std::vector<Similarity> similarity_funcs) {
    double total = 0;
    
    for (size_t k = 0; k < similarity_funcs.size(); k++) {
      total += similarity_funcs[k](a[k], b[k]);
    }

    return total;
  }
}
