#ifndef MODULES_DIALOGUE_PARSER_H_
#define MODULES_DIALOGUE_PARSER_H_

#include <string>
#include <vector>
#include <map>

namespace decoda {

  typedef std::vector<std::string> Word;
  typedef std::vector<Word> Turn;
  typedef std::vector<Turn> Dialogue;
  
  class DialogueParser {
  private:
    std::vector<std::string> files;
    std::string directory;

    Word ReadLine(std::string line);
    Turn ReadTurn(std::vector<std::string> str_turn);
    
  public:
    std::vector<Dialogue> dialogues;
    std::map<std::string, int> dialogue2index;
    std::vector<std::string> index2dialogue;
    
    DialogueParser(const std::string dirPath);
    void ReadAll();
    bool ReadNextDialogue();
    void PrintDialogue(int index);
  };

  enum Column {
    kDialogue = 0,
    kTurn = 1,
    kId = 2,
    kForm = 3,
    kDisfluency = 4,
    kPos = 5,
    kNamedEntity = 6,
    kChunk = 7,
    kDeprel = 8,
    kHead = 9,
    kIdText = 10,
    kLemma = 11,
    kFeats = 12,
    kSpeaker = 13,
    kStart = 14,
    kEnd = 15,
    kDelay = 16,
    kEndTurn = 17,
    kLexicalUnit = 18,
    kFrameElement = 19,
    kSpkResolved = 20,
    kAct = 21,
    kPolarity = 22,
    kNB_COLUMNS = 23
  };

} // namespace decoda

#endif /* MODULES_DIALOGUE_PARSER_H_ */
