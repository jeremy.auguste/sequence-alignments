#include "sequence_alignment.hh"

#include <iostream>
#include <exception>
#include <utility>
#include <stack>
#include <iomanip>
#include <unordered_map>
#include <unordered_set>
#include <boost/lexical_cast.hpp>

namespace decoda {

  AlignmentFunc AlignmentFuncFromString(std::string str) {
    if (str == "smithwaterman")
      return SmithWaterman;
    else if (str == "needlemanwunsch")
      return NeedlemanWunsch;

    throw std::invalid_argument(str + " is not a valid alignment method");
  }
  
  double SmithWaterman(Sequence &a, Sequence &b,
		       std::vector<Similarity> &similarity_funcs,
		       double init_gap_penality,
		       double extend_gap_penality) {
    double max_score = 0.0;
    const int kN = a.size()+1;
    const int kM = b.size()+1;
    
    double *matches = new double[kN*kM]();
    double *prefixes_gap_b = new double[kN*kM]();
    double *prefixes_gap_a = new double[kN*kM]();

    for (int i = 1; i < kN; ++i) {
      for (int j = 1; j < kM; ++j) {
	const int current = i*kM+j;

	double match = matches[(i-1)*kM+(j-1)] + TotalSimilarity(a[i-1],b[j-1],similarity_funcs);

	double pgbi = matches[(i-1)*kM+j] - (init_gap_penality + extend_gap_penality);
	double pgbe = prefixes_gap_b[(i-1)*kM+j] - extend_gap_penality;
	prefixes_gap_b[current] = std::max(pgbi, pgbe);

	double pgai = matches[i*kM+(j-1)] - (init_gap_penality + extend_gap_penality);
	double pgae = prefixes_gap_b[i*kM+(j-1)] - extend_gap_penality;
	prefixes_gap_a[current] = std::max(pgai, pgae);	
	
	// double deletion = 0;
	// for (int k = 1; k < i+1; k++) {
	//   double kdel = matches[(i - k)*kM+j] - init_gap_penality-k*extend_gap_penality;
	//   if (kdel > deletion) {
	//     deletion = kdel;
	//   }
	// }

	// double insertion = 0;
	// for (int k = 1; k < j+1; k++) {
	//   double kins = matches[i*kM+(j - k)] - init_gap_penality-k*extend_gap_penality;
	//   if (kins > insertion) {
	//     insertion = kins;
	//   }
	// }

	double max_indel = std::max(prefixes_gap_b[current], prefixes_gap_a[current]);
	double max_match_indel = std::max(match, max_indel);
	matches[current] = std::max(0.0, max_match_indel);

	if (max_score < matches[current]) {
	  max_score = matches[current];
	}
      }
    }

    delete[] matches;
    delete[] prefixes_gap_a;
    delete[] prefixes_gap_b;
    
    return max_score;
  }

  double NeedlemanWunsch(Sequence &a, Sequence &b,
  			 std::vector<Similarity> &similarity_funcs,
  			 double init_gap_penality,
  			 double extend_gap_penality) {
    const int kN = a.size()+1;
    const int kM = b.size()+1;
    
    double *matches = new double[kN*kM]();

    for (int i = 1; i < kN; i++) {
      for (int j = 1; j < kM; j++) {

	double match = matches[(i-1)*kM+(j-1)] + TotalSimilarity(a[i-1],b[j-1],similarity_funcs);
	double deletion = 0;
	for (int k = 1; k < i+1; k++) {
	  double kdel = matches[(i - k)*kM+j] - init_gap_penality-k*extend_gap_penality;
	  if (kdel > deletion) {
	    deletion = kdel;
	  }
	}

	double insertion = 0;
	for (int k = 1; k < j+1; k++) {
	  double kins = matches[i*kM+(j - k)] - init_gap_penality-k*extend_gap_penality;
	  if (kins > insertion) {
	    insertion = kins;
	  }
	}

	double max_indel = std::max(deletion, insertion);
	matches[i*kM+j] = std::max(match, max_indel);

      }
    }

    double score = matches[(kN-1)*kM+(kM-1)];

    delete[] matches;
    
    return score;
  }
  

  double AlignmentSimilarity(Sequence &a, Sequence &b,
			     std::vector<Similarity> &similarity_funcs,
			     double init_gap_penality,
			     double extend_gap_penality,
			     AlignmentFunc alignment_func) {
    double ab = alignment_func(a,b, similarity_funcs,
				init_gap_penality,
				extend_gap_penality);
    double aa = alignment_func(a,a, similarity_funcs,
				init_gap_penality,
				extend_gap_penality);
    double bb = alignment_func(b,b, similarity_funcs,
				init_gap_penality,
				extend_gap_penality);
    
    return ab / std::max(aa, bb);
  }

  double GetBestAlignmentPath(double *matches, int *paths, bool *visited,
			      const int kN, const int kM,
			      std::string &cigar,
			      Coordinates &topleft) {
    int max_i = 0;
    int max_j = 0;
    double score = 0.0;
    for (int i = 1; i < kN; i++) {
      for (int j = 1; j < kM; j++) {
	if (matches[i*kM+j] > matches[max_i*kM+max_j]) {
	  score = matches[i*kM+j];
	  max_i = i;
	  max_j = j;
	} else if (matches[i*kM+j] == matches[max_i*kM+max_j]) {
	  if (i+j < max_i+max_j) {
	    max_i = i;
	    max_j = j;
	  } else if (i+j == max_i+max_j && i < max_i) {
	    max_i = i;
	    max_j = j;
	  }
	}
      }
    }

    int i = max_i;
    int j = max_j;
    int min_i = i;
    int min_j = j;
    // TODO: Maybe change that by looking at the entire "tree" (cf article)
    cigar = "";
    while (paths[i*kM+j] != kPathNone) {
      min_i = i;
      min_j = j;
      visited[i*kM+j] = true;
      // std::ostringstream ss;
      // ss << std::fixed << std::setprecision(0);
      // ss << matches[i*kM+j]*10;
      // std::string temp = ss.str();
      // std::reverse(temp.begin(), temp.end());
      // cigar += temp;
      if (paths[i*kM+j] & kPathDiagonal) {
	if (matches[i*kM+j] < matches[(i-1)*kM+(j-1)])
	  cigar += "X";
	else
	  cigar += "M";
	i--;
	j--;
      } else if (paths[i*kM+j] & kPathUp) {
	cigar += "D";
	i--;
      } else {
	cigar += "I";
	j--;
      }
    }

    topleft = {min_i, min_j};
    std::reverse(cigar.begin(), cigar.end());
    
    return score;
  }

  bool UpdateCell(Sequence &a, Sequence &b,
		  std::vector<Similarity> &similarity_funcs,
		  double init_gap_penality,
		  double extend_gap_penality,
		  double *matches, double *prefixes_gap_a, double *prefixes_gap_b,
		  int *paths, bool *already_checked, const int kM,
		  int i, int j) {
    int current = i*kM+j;

    double match = 0.0;
    if (!already_checked[current]) {
      match = matches[(i-1)*kM+(j-1)] + TotalSimilarity(a[i-1],b[j-1],similarity_funcs);
    }

    double pgai = matches[(i-1)*kM+j] - (init_gap_penality + extend_gap_penality);
    double pgae = prefixes_gap_b[(i-1)*kM+j] - extend_gap_penality;
    double pga = std::max(pgai, pgae);

    double pgbi = matches[i*kM+(j-1)] - (init_gap_penality + extend_gap_penality);
    double pgbe = prefixes_gap_b[i*kM+(j-1)] - extend_gap_penality;
    double pgb = std::max(pgbi, pgbe);

    double score = std::max(std::max(0.0, match), std::max(pga, pgb));

    paths[current] = kPathNone;
    if (score != 0.0) {
      if (score == match)
	paths[current] |= kPathDiagonal;
      if (score == pga)
	paths[current] |= kPathUp;
      if (score == pgb)
	paths[current] |= kPathLeft;
    }
    
    if (score != matches[current] ||
	pgb != prefixes_gap_b[current] ||
	pga != prefixes_gap_a[current]) {
      matches[current] = score;
      prefixes_gap_b[current] = pgb;
      prefixes_gap_a[current] = pga;
      
      return true;
    }

    return false;
  }

  void RecomputeMatrices(Sequence &a, Sequence &b,
			 std::vector<Similarity> &similarity_funcs,
			 double init_gap_penality,
			 double extend_gap_penality,
			 double *matches, double *prefixes_gap_a, double *prefixes_gap_b,
			 int *paths, bool *already_checked, const int kN, const int kM,
			 Coordinates &topleft) {


    int prev_n = 0;
    int prev_m = 0;
    for (int i = topleft.first, j = topleft.second; i < kN && j < kM ; ++i, ++j) {
      bool updated = UpdateCell(a, b, similarity_funcs, init_gap_penality, extend_gap_penality,
				matches, prefixes_gap_a, prefixes_gap_b, paths, already_checked,
				kM, i, j);
      bool source_updated = updated;
      int k;
      for (k = i+1; (k < prev_n || updated) && k < kN; ++k) {
	updated = UpdateCell(a, b, similarity_funcs, init_gap_penality, extend_gap_penality,
			     matches, prefixes_gap_a, prefixes_gap_b, paths, already_checked,
			     kM, k, j);
      }
      prev_n = k;

      updated = source_updated;
      int l;
      for (l = j+1; (l < prev_m || updated) && l < kM; ++l) {
	updated = UpdateCell(a, b, similarity_funcs, init_gap_penality, extend_gap_penality,
			     matches, prefixes_gap_a, prefixes_gap_b, paths, already_checked,
			     kM, i, l);
      }
      prev_m = l;
    }
  }

  std::vector<Alignment> WatermanEggert(Sequence &a, Sequence &b,
					std::vector<Similarity> &similarity_funcs,
					double init_gap_penality,
					double extend_gap_penality,
					double min_score) {
    std::vector<Alignment> sequences;
    const int kN = a.size()+1;
    const int kM = b.size()+1;
    
    double *matches = new double[kN*kM]();
    double *prefixes_gap_b = new double[kN*kM]();
    double *prefixes_gap_a = new double[kN*kM]();
    int *paths = new int[kN*kM]();
    bool *visited = new bool[kN*kM]();
    

    for (int i = 1; i < kN; ++i) {
      for (int j = 1; j < kM; ++j) {
	const int current = i*kM+j;

	double match = matches[(i-1)*kM+(j-1)] + TotalSimilarity(a[i-1],b[j-1],similarity_funcs);

	double pgbi = matches[(i-1)*kM+j] - (init_gap_penality + extend_gap_penality);
	double pgbe = prefixes_gap_b[(i-1)*kM+j] - extend_gap_penality;
	prefixes_gap_a[current] = std::max(pgbi, pgbe);

	double pgai = matches[i*kM+(j-1)] - (init_gap_penality + extend_gap_penality);
	double pgae = prefixes_gap_b[i*kM+(j-1)] - extend_gap_penality;
	prefixes_gap_b[current] = std::max(pgai, pgae);	
	
	double max_indel = std::max(prefixes_gap_b[current], prefixes_gap_a[current]);
	double max_match_indel = std::max(match, max_indel);
	matches[current] = std::max(0.0, max_match_indel);

	paths[current] = kPathNone;
	if (matches[current] != 0.0) {
	  if (matches[current] == match)
	    paths[current] |= kPathDiagonal;
	  if (matches[current] == prefixes_gap_a[current])
	    paths[current] |= kPathUp;
	  if (matches[current] == prefixes_gap_b[current])
	    paths[current] |= kPathLeft;
	}

      }
    }

    do {
      std::string cigar;
      Coordinates topleft;
      double score = GetBestAlignmentPath(matches, paths, visited, kN, kM, cigar, topleft);
      
      if (score < min_score) {
	break;
      }
      Alignment alignment(a, b, score, {topleft.first-1,topleft.second-1}, cigar);
      sequences.push_back(alignment);

      RecomputeMatrices(a, b, similarity_funcs,
			init_gap_penality, extend_gap_penality,
			matches, prefixes_gap_a, prefixes_gap_b, paths, visited, kN, kM, topleft);
      
    } while(true);

    delete[] matches;
    delete[] prefixes_gap_a;
    delete[] prefixes_gap_b;
    delete[] paths;
    delete[] visited;
    
    return sequences;
  }
}
