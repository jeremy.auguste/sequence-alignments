#ifndef MODULES_SEQUENCE_EXTRACTORS_H_
#define MODULES_SEQUENCE_EXTRACTORS_H_

#include "dialogue_parser.hh"

#include <vector>
#include <string>
#include <boost/variant.hpp>


namespace decoda {

  using ExtractorTypes = boost::variant<double, int, std::string>;
  typedef std::vector<ExtractorTypes> (*Extractor)(Dialogue &dialogue);
  
  std::vector<ExtractorTypes> DialogueActs(Dialogue &dialogue);
  std::vector<ExtractorTypes> TurnLength(Dialogue &dialogue);
  std::vector<ExtractorTypes> TurnMeanPolarity(Dialogue &dialogue);
  std::vector<ExtractorTypes> TurnMeanPolarityDiscrete(Dialogue &dialogue);
  std::vector<ExtractorTypes> Speaker(Dialogue &dialogue);
  std::vector<ExtractorTypes> ResponseTime(Dialogue &dialogue);
}

#endif /* MODULES_SEQUENCE_EXTRACTORS_H_ */
