#ifndef MODULES_DIALOGUE_SEQUENCER_H_
#define MODULES_DIALOGUE_SEQUENCER_H_

#include "sequence_extractors.hh"
#include "dialogue_parser.hh"

#include <vector>
#include <string>

namespace decoda {

  typedef std::vector<std::vector<ExtractorTypes> > Sequence;
  
  class DialogueSequencer {
  private:
    std::vector<Extractor> extractors;
  public:
    DialogueSequencer(std::vector<Extractor> extractor);
    Sequence Transform(Dialogue &dialogue);
    std::vector<Sequence> TransformAll(std::vector<Dialogue> &dialogues);
  };

  
}

#endif /* MODULES_DIALOGUE_SEQUENCER_H_ */
