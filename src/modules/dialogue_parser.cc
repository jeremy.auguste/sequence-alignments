#include "dialogue_parser.hh"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <regex>

#include <stdio.h>
#include <dirent.h>

namespace decoda {
  DialogueParser::DialogueParser(const std::string dirPath) : directory(dirPath) {
    DIR *dir;
    struct dirent *entry;

    if ((dir = opendir(dirPath.c_str())) == NULL) {
      perror("DialogueParser");
      throw std::invalid_argument("Invalid directory");
    }

    std::regex file_regex("^[0-9]+_RATP_SCD_[0-9]+\\.tsv$");

    while ((entry = readdir(dir)) != NULL) {
      std::string name = entry->d_name;
      
      // if (name.find("_RATP_SCD_") != std::string::npos) {
      if (std::regex_match(name, file_regex)) {
      	// std::cerr << "File OK: " << name << std::endl;
      	files.push_back(name);
      }
    }

    closedir(dir);
  }
  
  void DialogueParser::ReadAll() {
    while (ReadNextDialogue());
  }

  Word DialogueParser::ReadLine(std::string line) {
    Word word;

    std::istringstream iss(line);
    std::string token;

    while (getline(iss, token , '\t')) {
      word.push_back(token);
    }

    return word;
  }

  Turn DialogueParser::ReadTurn(std::vector<std::string> str_turn) {
    Turn turn;

    for (auto line : str_turn) {
      Word word = ReadLine(line);

      if (word.size() != kNB_COLUMNS) {
	throw std::runtime_error("Wrong number of columns!");
      }
      
      turn.push_back(word);
    }
    
    return turn;
  }
  
  bool DialogueParser::ReadNextDialogue() {
    bool validDialogue = true;
    Dialogue dialogue;
    std::string dialogueName;
    
    do {
      if (files.empty()) {
	return false;
      }
    
      auto current = files.back();
      files.pop_back();
      std::string dlg_path = directory + "/" + current;
      size_t lastindex = current.find_last_of("."); 
      dialogueName = current.substr(0, lastindex); 
      
      std::ifstream dlg_file(dlg_path);
      std::string line;
      std::vector<std::string> str_turn;

      dialogue.clear();
      
      while (std::getline(dlg_file, line)) {
	if (line.empty()) {
	  if (!str_turn.empty()) {
	    try {
	      Turn turn = ReadTurn(str_turn);
	      dialogue.push_back(turn);
	    } catch (std::runtime_error &e) {
	      std::cerr << "File '" << current << "' has an error ("
			<< e.what() << "). Skipping..." << std::endl;
	      validDialogue = false;
	      break;
	    }
	    str_turn.clear();
	  }

	  continue;
	}
	str_turn.push_back(line);
      }

      if (!str_turn.empty() && validDialogue) {
	try {
	  Turn turn = ReadTurn(str_turn);
	  dialogue.push_back(turn);
	} catch (std::runtime_error &e) {
	  std::cerr << "File '" << current << "' has an error ("
		    << e.what() << "). Skipping..." << std::endl;
	  validDialogue = false;
	}
	str_turn.clear();
      }
      
      dlg_file.close();
    } while (!validDialogue);
    
    dialogues.push_back(dialogue);
    dialogue2index[dialogueName] = dialogues.size() - 1;
    index2dialogue.push_back(dialogueName);
   
    return true;
  }

  void DialogueParser::PrintDialogue(int index) {
    
  }
  
}
